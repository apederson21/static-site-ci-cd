module.exports = {
    create: create
}

function create(siteContent, imgs) {
    if (siteContent && !document.getElementById(siteContent.key)) { // only append once!
        let div = document.createElement('div');
        div.id = siteContent.key;
        div.classList.add('menu');
        let anchor = document.createElement('a');
        anchor.name = siteContent.key;
        div.appendChild(anchor);
        let h2 = document.createElement('h2');
        let itemsDiv = document.createElement('div');
        if (siteContent.heading) {
            h2.innerHTML = siteContent.heading;
            append(h2, div);
        }

        // loop contents
        if (siteContent.items) {
            let img = document.createElement('img');
            let h3 = document.createElement('h3');
            siteContent.items.forEach((item) => {
                if (item.image) {
                    img.src = `./images/${siteContent.image}`;
                    append(img, div);
                }
                if (item.title) {
                    h3.innerHTML = item.title;
                    append(h3, div);
                }
                if (item.contents) {
                    item.contents.forEach((itemContent) => {
                        let p = document.createElement('p');
                        if (itemContent.name) {
                            p.innerHTML = (`<strong>${itemContent.name}</strong><br />`);
                        }
                        if (itemContent.description) {
                            p.innerHTML = p.innerHTML + (itemContent.description);
                        }
                        append(p, itemsDiv);
                    });
                    append(itemsDiv, div);
                }
            });
        }

        function append(src, target) {
            target.appendChild(src);
        }

        document.getElementById('content').appendChild(div); // append to main
    }
}
