module.exports = {
    create: create
}

function create(siteContent, imgs) {
    if (siteContent && !document.getElementById(siteContent.key)) { // only append once!
        let div = document.createElement('div');
        div.id = siteContent.key;
        let anchor = document.createElement('a');
        anchor.name = siteContent.key;
        div.appendChild(anchor);
        let img = document.createElement('img');
        let h2 = document.createElement('h2');
        let p = document.createElement('p');

        if (siteContent.image) {
            /*
                take the image data, i.e.: 'foo.png',
                parse out the extension and prefix,
                then load URL from the bundled imgs object
            */
            let imgExtension = siteContent.image.match(/\.[0-9a-zA-Z]+$/gi)[0];
            let imgPrefix = siteContent.image.replace(imgExtension, '');
            imgExtension = imgExtension.substr(1);
            img.src = imgs[imgPrefix][imgExtension];
            append(img);
        }

        if (siteContent.heading) {
            h2.innerHTML = siteContent.heading;
            append(h2);
        }

        if (siteContent.text) {
            p.innerHTML = siteContent.text;
            append(p);
        }

        function append(src) {
            div.appendChild(src);
        }

        document.getElementById('content').appendChild(div); // append to main
    }
}
