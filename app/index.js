'use strict';

import './styles/styles.scss';
import imgs from './images/*.*';

// build navigation
const navLinks = require('./data/links.json');
const navLinkKeys = Object.keys(navLinks);
if (navLinkKeys && document.getElementsByTagName('nav').length === 0) {
    let navContainer = document.createElement('nav');
    navLinkKeys.forEach((key) => {
        let inner = document.createElement('span');
        inner.innerHTML = navLinks[key].text; // populate nav text
        inner.addEventListener('click', () => {
            navigate(navLinks[key].link);
        }); // add event
        navContainer.appendChild(inner);
    });
    document.getElementById('content').appendChild(navContainer); // append
    
    /**
     * Navigate page to destination
     * @param {string} dest 
     */
    function navigate(dest) {
        window.location = `#${dest}`;
    }
}

// build site content
const siteContent = require('./data/siteContent.json');
const siteContentKeys = Object.keys(siteContent);
if (siteContentKeys) {
    const templates = {
        default: require('./templates/default'),
        menu: require('./templates/menu')
    };
    siteContentKeys.forEach((key) => {
        // check for template
        let template = siteContent[key].template || 'default';
        templates[template].create(siteContent[key], imgs);
    });
}
