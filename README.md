# Static Site Deployed to S3

## What is this?
This project builds a simple <a href="http://static-site-ci-cd.s3-website-us-west-2.amazonaws.com/">static site</a> and includes GitLab CI/CD configuration to deploy directly to s3.

## Setup
1. Clone the repo `git clone git@gitlab.com:apederson21/static-site-ci-cd.git`
2. Ensure you have NodeJs v10.7 installed
3. Install dependencies `npm install`

## Commands
`npm run start` will bundle the code and start the dev server at localhost:1234 with hot module replacement.<br />
`npm run build` will create a production build of the code and output to the dist folder.

## Dependencies
For bundling and dev purposes I'm using https://parceljs.org/. For Sass I'm using node-sass.

## Code Structure
All the development code is located in the app directory. `index.html` and `index.js` are the main entrypoints for the bundle.

`data` and `templates` directories work together as the blueprints to make the body content divs for the static site. `data/links.json` holds the nav information while `siteContent.json` has the site text.

`.gitlab-ci.yml` in the root contains the build and deploy instructions for the pipeline and references the `scripts` directory for shell commands.