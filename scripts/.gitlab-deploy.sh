if [ -d "dist" ]; then
  # install awscli
  echo "installing awscli..."
  pip install awscli

  echo "pushing dist contents to s3..."
  cd dist
  aws s3 sync . s3://$S3_BUCKET_NAME_PROD
fi
