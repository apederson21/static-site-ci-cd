# output debugging items
nodeV=$(node -v)
echo "node -v $nodeV"

npmV=$(npm -v)
echo "npm -v $npmV"

echo "installing dependencies..."
npm install

echo "building the code..."
npm run build
